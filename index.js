
// Bài 1
function showSalary(){
    var tagSalaryPerDay = document.getElementById('salary-per-day').value;
    var tagWorkDay = document.getElementById('work-day').value;
    var finalSalary = tagSalaryPerDay * tagWorkDay;
    var tagShowSalary = document.getElementById('final-salary');
    tagShowSalary.innerHTML = finalSalary.toLocaleString(); 
    // .innerHTML used for div tag with string fill
    // .value used for input tag that user input value/string
}

// Bài 2
function avgNumber(){
    var tagNumber1 = document.getElementById('number1').value;
    var tagNumber2 = document.getElementById('number2').value;
    var tagNumber3 = document.getElementById('number3').value;
    var tagNumber4 = document.getElementById('number4').value;
    var tagNumber5 = document.getElementById('number5').value;
    var sumNumber = Number(tagNumber1) + Number(tagNumber2) + Number(tagNumber3) + Number(tagNumber4) + Number(tagNumber5);
    // All value record will be string type, take the value*1 or Number(value) to make it becomes number type
    var averageNumber = sumNumber / 5;
    var showAvgNumber = document.getElementById('show-avgnumber');
    showAvgNumber.innerHTML = averageNumber;
}

// Bài 3
function convertDollar(){
    var tagInputDollar = document.getElementById('input-dollar').value;
    var tagExchangeRate = 23500;
    var showConvertedBill = document.getElementById('convert-dollar');
    var convertedBill = tagInputDollar * tagExchangeRate;
    showConvertedBill.innerHTML = convertedBill.toLocaleString() + ' VND';
}

// Bài 4
var resultMenu = document.getElementById('result-menu')
function findArea(){
    var tagRetWidth = document.getElementById('ret-width').value;
    var tagRetHeight = document.getElementById('ret-height').value;
    var retArea = tagRetWidth * tagRetHeight;
    var findAreaFinal = document.getElementById('final-result');
    findAreaFinal.innerHTML = retArea + ' cm2';
    resultMenu.className = 'mt-2 px-3 py-2 alert alert-success w-50 rounded';
}
function findPerimeter(){
    var tagRetWidth = document.getElementById('ret-width').value;
    var tagRetHeight = document.getElementById('ret-height').value;
    var retPerimeter = (Number(tagRetWidth) + Number(tagRetHeight)) * 2;
    var findPerimeterFinal = document.getElementById('final-result');
    findPerimeterFinal.innerHTML = retPerimeter + ' cm';
    resultMenu.className = 'mt-2 px-3 py-2 alert alert-warning w-50 rounded';
}

// Bài 5
function calPlus(){
    var tagTwoNumber = document.getElementById('two-number').value;
    var numberTen = Math.floor(tagTwoNumber / 10);
    var numberUnit = tagTwoNumber % 10;
    var finalResult = Number(numberTen) + Number(numberUnit);
    var showPlusResult = document.getElementById('cal-plus');
    showPlusResult.innerHTML = finalResult;
}
